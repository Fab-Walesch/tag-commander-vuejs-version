import Vue from 'vue'

import router from './route.js'

import App from './App.vue'

new Vue({
  el: '#app',
  router,
  render: h => h(App)
  
})