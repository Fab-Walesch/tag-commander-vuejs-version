let cartItem = {
    props: ['items'],
    data() {
        return {
            index: ''
        }
    },
    methods: {
        removeFromCart(index) {
            this.items.splice(index, 1)
        },
        removeCartQuantity(index) {
            if (this.items[index].quantity === 1) {
                this.removeFromCart(index)
             } else {
                this.items[index].quantity -= 1
            }
        },
        addCartQuantity(index) {
            this.items[index].quantity += 1
        }
    },
    template: `<div>
                <ul :items="items">
                    <li v-for="(item, index) in items">
                        <h5>{{ item.name }}</h5>
                        <div class="cart-quantity">
                            <div class="grouped cart-group">
                                <button class="sm-button red-500" @click="removeCartQuantity(index)">
                                    <i class="material-icons quantity">
                                        remove
                                    </i>
                                </button>
                                <span>{{ item.quantity }}</span>
                                <button class="sm-button green-500" @click="addCartQuantity(index)">
                                    <i class="material-icons quantity">
                                        add
                                    </i>
                                </button>
                            </div>
                            <div class="cart-item-price">
                                {{ item.quantity * item.price }} {{ item.currency }}
                            </div>
                        </div>
                    </li>
                </ul>
               </div>`
}

export default cartItem