let grandTotal = {
    props: ['items'],
    data() {
      return {
          currency: '€'
      }  
    },
    computed: {
        cartGrandTotal() {
            let total = 0
            this.items.forEach ((item) => {
                total += (item.price * item.quantity)
            });
        return total;
        }
    },
    template:`<span class="grand-total">{{ cartGrandTotal }} {{ currency }}</span>`
}

export default grandTotal