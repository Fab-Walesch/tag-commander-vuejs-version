let pageItemName = {
    props: ['product'],
    name: 'PageItemName',
    template: `<h2>{{ product.name }}</h2>`
}

export default pageItemName