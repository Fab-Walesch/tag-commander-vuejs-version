let checkout = {
    props: ['items'],
    methods: {
        checkout(items) {
            this.$emit('clicked')
        }
    },
    template:`<button class="button green-500 buy-button" @click="checkout(items)">
                Buy
                <i class="material-icons quantity">
                    done
                </i>
            </button>`
}

export default checkout