import { shallowMount } from '@vue/test-utils';
import Shop from '../src/components/Shop.vue';

let buyMsg = false

describe ('Message Initial', () => {
    it('Message doesn\'t active', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    buyMsg
                }
            }
        })
        expect(wrapper.find('.buyMsg').exists()).toBeFalsy()
    })
})

describe ('Activated Message', () => {
    it('Activating Message when click Buy button', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    buyMsg
                }
            }
        })
        expect(wrapper.find('msg-card-displayed', () => {
            wrapper.find('checkout').trigger('click')
            expect(wrapper.find('.buyMsg').exists()).toBeTruly()
        }))
    })
})

describe ('Message Active', () => {
    it('Message Shop cart is activ', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    buyMsg: true
                }
            }
        })
        expect(wrapper.find('.msg-card-displayed', () => {
            expect(wrapper.find('.buyMsg').exists()).toBeTruly()
        }))
        expect(wrapper.find('msg-card-displayed', () => {
            wrapper.find('i').trigger('click')
            expect(wrapper.find('.buyMsg').exists()).toBeFalsy()
        }))
    })
})