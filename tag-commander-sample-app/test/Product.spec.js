import { shallowMount } from '@vue/test-utils';
import Shop from '../src/components/Shop.vue';

const product = {id: 4, name: 'Test Name 1', quantity: 0, price: 30, currency: '$'}

describe ('Product Object', () => {
    it('Product does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product   
                }  
            }
        })
        expect(wrapper.find('.product', () => {
            expect(wrapper.find('id').int()).toEqual(4)
            expect(wrapper.find('name').text()).toEqual('Test Name 1')
            expect(wrapper.find('quantity').int()).toEqual(0)
            expect(wrapper.find('price').int()).toEqual(30)
            expect(wrapper.find('currency').text()).toEqual('$')
        }))
    })
})

describe ('Binding Product Name', () => {
    it('Product Name does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product   
                }  
            }
        })
        expect(wrapper.find('.item-name', () => {
            expect(wrapper.find('name').text()).toEqual('Test Name 1')
        }))
    })
})

describe ('Binding Product Quantity', () => {
    it('Product Quantity does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product   
                }  
            }
        })
        expect(wrapper.find('page-item', () => {
            expect(wrapper.find('quantity').int()).toEqual(0)
        }))
    })
})

describe ('Binding Product Price', () => {
    it('Product Price does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product   
                }  
            }
        })
        expect(wrapper.find('page-item', () => {
            expect(wrapper.find('price').int()).toEqual(30)
        }))
    })
})

describe ('Binding Product Currency', () => {
    it('Product Currency does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product   
                }  
            }
        })
        expect(wrapper.find('page-item', () => {
            expect(wrapper.find('currency').text()).toEqual('$')
        }))
    })
})

describe ('Increment product quantity', () => {
    it('Increment quantity when button is clicked', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product : {quantity: 0}
                }
            }
        })
        expect(wrapper.find('.page-item', () => {
            expect(wrapper.find('span').text()).toEqual('0'), () => {
                wrapper.find('.green-500').trigger('click')
                expect(wrapper.find('span').text()).toMatch('1')
            }
        }))
    })
})

describe ('Decrement product quantity', () => {
    it('Decrement quantity when button is clicked', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product : {quantity: 1}
                }
            }
        })
        expect(wrapper.find('.page-item', () => {
            expect(wrapper.find('span').text()).toEqual('1'), () => {
                wrapper.find('.red-500').trigger('click')
                expect(wrapper.find('span').text()).toMatch('0')
            }
        }))
    })
})

describe ('Add Product to Items', () => {
    it('Add product to cart', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product : {quantity: 1},
                    items : []
                }
            }
        })
        expect(wrapper.find('.page-item', () => {
            expect(wrapper.find('.cart-button').trigger('click'), () => {
                wrapper.find('span').text().toEqual('1')
                expect(wrapper.find('span').text()).toMatch('0')
            })
            expect(wrapper.find('.cart-button').trigger('click'), () => {
                wrapper.find('.price').text().toEqual('30')
                expect(wrapper.find('.price').text()).toMatch('0')
            })
            expect(wrapper.find('.cart-button').trigger('click'), () => {
                expect(wrapper.find('.product'), () => {
                    wrapper.find('.items')
                    expect(wrapper.find('.product').exists()).toMatch('.item')
                })
            })
        }))
    })
})