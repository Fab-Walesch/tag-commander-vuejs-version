import { shallowMount } from '@vue/test-utils';
import Shop from '../src/components/Shop.vue';

const items = [
                {id: 5, name: 'Test Name 2', quantity: 9, price: 25, currency: '$'},
                {id:6, name: 'Test Name 3', quantity: 6, price: 15, currency: '$'}
            ]

describe ('item in Items', () => {
    it('item in Items does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.items', () => {
            expect(wrapper.find('id').int()).toEqual(5)
            expect(wrapper.find('id').int()).toEqual(6)
            expect(wrapper.find('name').text()).toEqual('Test Name 2')
            expect(wrapper.find('name').text()).toEqual('Test Name 3')
            expect(wrapper.find('quantity').int()).toEqual(9)
            expect(wrapper.find('quantity').int()).toEqual(6)
            expect(wrapper.find('price').int()).toEqual(25)
            expect(wrapper.find('price').int()).toEqual(15)
            expect(wrapper.find('currency').text()).toEqual('$')
        }))
    })
})

describe ('Binding item id', () => {
    it('item id does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('id').int()).toEqual(5)
            expect(wrapper.find('id').int()).toEqual(6)
        }))
    })
})

describe ('Binding item name', () => {
    it('item name does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('name').text()).toEqual('Test Name 2')
            expect(wrapper.find('name').text()).toEqual('Test Name 3')
        }))
    })
})

describe ('Binding item quantity', () => {
    it('item quantity does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('quantity').int()).toEqual(9)
            expect(wrapper.find('quantity').int()).toEqual(6)
        }))
    })
})

describe ('Binding item price', () => {
    it('item price does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('price').int()).toEqual(25)
            expect(wrapper.find('price').int()).toEqual(15)
        }))
    })
})

describe ('Binding item currency', () => {
    it('item currency does exist', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('currency').text()).toEqual('$')
        }))
    })
})

describe ('Increment item quantity', () => {
    it('Increment quantity when button is clicked', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('span').text()).toEqual('0'), () => {
                wrapper.find('.green-500').trigger('click')
                expect(wrapper.find('span').text()).toMatch('1')
            }
        }))
    })
})

describe ('Decrement item quantity', () => {
    it('Decrement quantity when button is clicked', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    product : {quantity: 1}
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('span').text()).toEqual('1'), () => {
                wrapper.find('.red-500').trigger('click')
                expect(wrapper.find('span').text()).toMatch('0')
            }
        }))
    })
})

describe ('Buy Items', () => {
    it('Buy Items when button is clicked', () => {
        const wrapper = shallowMount(Shop, {
            data() {
                return {
                    items
                }
            }
        })
        expect(wrapper.find('.cart-item', () => {
            expect(wrapper.find('.buy-button').trigger('click'), () => {
                expect(wrapper.find('.items'), () => {
                    expect(wrapper.find('.item').exists()).toMatch('[]')
                })
            })
        }))
    })
})